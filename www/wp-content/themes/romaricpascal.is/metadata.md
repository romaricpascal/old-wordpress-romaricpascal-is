Theme Name: romaricpascal.is
Theme URI: https://github.com/rhumaric/romaricpascal.is
Author: Romaric Pascal <hello@romaricpascal.is>
Author URI: https://romaricpascal.is
Description: Wordpress theme
Version: 1.0
License: UNLICENSED
