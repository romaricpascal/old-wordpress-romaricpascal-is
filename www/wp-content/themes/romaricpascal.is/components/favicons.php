<link rel="apple-touch-icon" sizes="180x180" href="<?= get_theme_file_uri('/assets/images/favicons/apple-touch-icon.png') ?>">
<link rel="icon" type="image/png" sizes="32x32" href="<?= get_theme_file_uri('/assets/images/favicons/favicon-32x32.png') ?>">
<link rel="icon" type="image/png" sizes="16x16" href="<?= get_theme_file_uri('/assets/images/favicons/favicon-16x16.png')?>">
<link rel="manifest" href="<?= get_theme_file_uri('/assets/images/favicons/manifest.json')?>">
<link rel="mask-icon" href="<?= get_theme_file_uri('/assets/images/favicons/safari-pinned-tab.svg')?>" color="#022950">
<link rel="shortcut icon" href="<?= get_theme_file_uri('/assets/images/favicons/favicon.ico')?>">
<meta name="msapplication-config" content="<?= get_theme_file_uri('/assets/images/favicons/browserconfig.xml')?>">
<meta name="theme-color" content="#ffc744">