<?php 
	$postType = get_post_type_object( $postTypeName );
	if ($craft) {
		$craftId = $craft->term_id;
	}
  	$query = rp_query_featured_posts($postTypeName, rp_get_archive_size($postTypeName), $craftId);
	$postListFormat = rp_get_postListFormat($postTypeName); ?>

<?php if ($query->have_posts()): ?>
<article class="<?= "{$classes}"; ?>"
	<?php if ($id) {echo "id='{$id}'";} ?>
	data-inview >
	<div class="l-sideBySide">
		<header class="l-sideBySide__header l-vertCentered u-vgap-firstLarge">
			<?php rp_render('archiveHeading/archiveHeading', 
			                ['postType' => $postType, 
			                 'next_id' => $next_id,
			                 'craft' => $craft,
			                 'headingLevel' => $headingLevel], 
			                [$postTypeName, rp_get($craft, 'slug')]); ?>
			<?php rp_render('archiveDescription/archiveDescription', 
			                ['postType' => $postType, 
			                 'craft' => $craft, 
			                 'classes' => 'u-show-xl'], 
			                [$postTypeName, rp_get($craft, 'slug')]); ?>
		</header>
		<?php rp_render('postList', [
		  'query' => $query, 
		  'classes' => "l-sideBySide__main u-mb-1",
		  'postType' => $postTypeName,
		  'craft' => $craft,
		  'size' => 800,
		  'format' => $postListFormat,
		  'headingLevel' => $headingLevel + 1 ], [$postTypeName, rp_get($craft, 'slug')]); ?>
	 </div>
	 <?php rp_render('archiveDescription/archiveDescription', 
			                ['postType' => $postType, 'craft' => $craft, classes => 'u-hide-xl'], 
			                [$postTypeName, rp_get($craft, 'slug')]); ?>
</article>
<?php endif; ?>