<h<?= $headingLevel;?> class="rp-LetteredHeading js-ScrollAnim rp-LetteredHeading-words">
	<?php if($next_id): ?>
		<a href="#<?= $next_id ?>">
	<?php endif; ?>
  <span class="rp-LetteredHeading__picture rp-LetteredHeading__part"></span>
  <span class="rp-LetteredHeading__their js-ScrollAnim__part rp-LetteredHeading__part">Their
  </span>
  <span class="rp-LetteredHeading__words js-ScrollAnim__part rp-LetteredHeading__part">words
  </span>
  <span class="rp-LetteredHeading__about js-ScrollAnim__part rp-LetteredHeading__part">about
  </span>
  <span class="rp-LetteredHeading__me js-ScrollAnim__part rp-LetteredHeading__part">me.
  </span>
  		<?php if($next_id): ?>
  		</a>
 		<?php endif; ?>
</h<?= $headingLevel;?>>
