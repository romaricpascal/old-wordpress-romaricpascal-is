<div class="<?= $classes; ?>">
	<p>T-shirts and other objects can be great supports for spreading your message: leaving a attendees with a memorable reminder of your event, spreading the word about what your company cares about or just as products of their own.</p>
        <p><a href="/proud-of/lettering">Lettering</a> helps object carry those messages. It tunes the letters shapes to match the desired personality. And because the letters are drawn specifically, it lets them fit the shape of the object neatly.</p>
        <p>Thinking of getting your message on a t-shirt, or maybe another support entirely? <a href="<?php echo is_front_page()? '#' : '/'; ?>happy-to-chat">Get in touch</a>!</p>
</div>