<div class="<?= $classes; ?>" >
<p>I learnt a lot from others sharing online. Maybe I can help sharing what I learn along the way too. You'll find articles about the tools &amp; techniques I use, my processes as well as case studies.</p>
<p>To make sure you don&rsquo;t miss anything, add the site to your RSS reader or follow me on <a href="http://twitter.com/romaricpascal">Twitter</a>/<a href="http://instagram.com/romaricpascal">Instagram</a>. </p>
</div>