<div class="<?= $classes; ?>">
	<p >Lettering is the art of drawing letters. It brings the letters to life and helps them say more than just the words they write: giving them a bespoke tone of voice, stressing key parts of their message and ensuring they fit their support nicely.</p>

	<p>This helps your message make the right impact to your audience:</p>
	<ul>
		<li>share what makes your project or business unique through its <a href="/proud-of/logo-brand-designs">logo and brand</a></li>
		<li>deliver memorable and impactful messages on <a href="/proud-of/print-illustration-designs">prints and in illustrations</a></li>
		<li>give <a href="/proud-of/tshirts-merch-designs">t-shirts and other objects</a> their own personality</li>
	</ul>

	<p>These are only a few examples. If you're looking to get some custom letters, feel free to <a href="/happy-to-chat">get in touch!</a></p>
</div>