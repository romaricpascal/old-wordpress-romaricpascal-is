<div class="<?= $classes; ?>">
	<p>From a single landing page for your last product to an interactive web app, websites come in all shapes and size. Each require a solution specific to its scope and budget.</p>
	<p>Over the last 8(ish) years, I&rsquo;ve been building websites big &amp; small (including this one). This allowed me to build up my skills and process for getting your website live. </p>
	<p>Whether you need a full website built, or some target front-end (HTML/CSS/JavaScript) help, I&rsquo;ll be happy to chat about how I can help. <a href="<?php echo is_front_page()? '#' : '/'; ?>happy-to-chat">Get in touch</a>!</p>
</div>