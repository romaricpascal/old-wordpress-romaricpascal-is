<div class="<?= $classes; ?>">
	<p>Your company or project is unique. You do things your way, believing in your own set of values. Interacting with you feels a certain way. Your brand needs to reflect this uniqueness and set you apart so your audience recognizes you. </p>
	<p><a href="/proud-of/lettering">Lettering</a> is all about uniqueness! Letters are drawn specifically for your logo. They communicate not just the name of your project or business, but also your values and personality, to your customers.</p>
	<p>Looking for bespoke letters to represent your project or company? <a href="<?php echo is_front_page()? '#' : '/'; ?>happy-to-chat">Get in touch</a>!</p>
</div>