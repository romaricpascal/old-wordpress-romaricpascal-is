<div class="rp-LegacyWarning <?= $classes; ?>">
	<p>These articles used to live on the old website. You might still find them useful, which is why I brought them here. For newer content, have a look at the <a href="/drawing-letters/">Artworks</a> or <a href="/writing-about/">Blog section</a>.</p>
</div>