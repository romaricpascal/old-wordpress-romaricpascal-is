<div class="<?= $classes; ?>">
	<p>Some messages need to be more memorable or impactful. Maybe a key quote in your article deserves extra attention. Or you want to make sure the message gets across on your poster.</p>
        <p><a href="/proud-of/lettering">Lettering</a> brings the message into the spotlight. Custom letter forms complement the message with a specific tone of voice. And the layout highlights key parts of the message and ensures it fits the shape of its support.</p>
        <p>Got a message that need to make a special impact? <a href="<?php echo is_front_page()? '#' : '/'; ?>happy-to-chat">Get in touch</a>!</p>
</div>