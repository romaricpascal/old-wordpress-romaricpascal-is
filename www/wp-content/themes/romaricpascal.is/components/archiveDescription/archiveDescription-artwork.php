<div class="<?= $classes; ?>">
	<p>It's not all about polished commercial works in the <a href="/proud-of/lettering">portfolio</a>. Discover personal works, work in progress and other sketches in here. Plus it's updated more often!</p>
        <p>To make sure you don't miss anything, add the site to your RSS reader or follow me on <a href="http://twitter.com/romaricpascal">Twitter</a>/<a href="http://instagram.com/romaricpascal">Instagram</a>. </p>
        
</div>