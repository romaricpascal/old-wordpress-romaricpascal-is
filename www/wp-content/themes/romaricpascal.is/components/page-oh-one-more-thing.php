<p class="rp-LetteredHeading js-ScrollAnim rp-LetteredHeading-thing" data-scrollAnim-name="oh">
 <?php if($next_id): ?>
 	<a href="#<?= $next_id ?>">
 <?php endif; ?>
  <span class="rp-LetteredHeading__picture rp-LetteredHeading__part"></span>
  <span class="rp-LetteredHeading__oh js-ScrollAnim__part rp-LetteredHeading__part">Oh!</span>
  <span class="rp-LetteredHeading__oneLast js-ScrollAnim__part rp-LetteredHeading__part">One last</span>
  <span class="rp-LetteredHeading__thing js-ScrollAnim__part rp-LetteredHeading__part">thing</span>  
  <span class="rp-LetteredHeading__etc js-ScrollAnim__part rp-LetteredHeading__part">…</span>
  <?php if($next_id): ?>
 	</a>
 <?php endif; ?>
</p>